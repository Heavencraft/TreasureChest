package com.mtihc.minecraft.treasurechest.v8.rewardfactory.rewards;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;

import com.mtihc.minecraft.treasurechest.v8.rewardfactory.IReward;
import com.mtihc.minecraft.treasurechest.v8.rewardfactory.RewardException;
import com.mtihc.minecraft.treasurechest.v8.rewardfactory.RewardFactory;
import com.mtihc.minecraft.treasurechest.v8.rewardfactory.RewardInfo;

import fr.hc.core.db.users.balance.UpdateUserBalanceQuery;
import fr.hc.core.exceptions.HeavenException;
import fr.hc.core.utils.chat.ChatUtil;
import fr.hc.rp.HeavenRP;
import fr.hc.rp.HeavenRPInstance;
import fr.hc.rp.db.users.RPUser;

public class MoneyRewardFactory extends RewardFactory
{
	private final HeavenRP plugin = HeavenRPInstance.get();

	@Override
	public String getLabel()
	{
		return "money";
	}

	@Override
	public String getGeneralDescription()
	{
		return "some amount of money.";
	}

	@Override
	public IReward createReward(RewardInfo info) throws RewardException
	{
		return new MoneyReward(this, info);
	}

	@Override
	public void createReward(CommandSender sender, String[] args, CreateCallback callback)
	{
		double money;

		try
		{
			money = Double.parseDouble(args[0]);
		}
		catch (final NullPointerException e)
		{
			callback.onCreateException(sender, args, new RewardException("Not enough arguments. Expected the amount of money.", e));
			return;
		}
		catch (final IndexOutOfBoundsException e)
		{
			callback.onCreateException(sender, args, new RewardException("Not enough arguments. Expected the amount of money.", e));
			return;
		}
		catch (final NumberFormatException e)
		{
			callback.onCreateException(sender, args, new RewardException("Expected the amount of money, instead of text.", e));
			return;
		}

		if (args.length > 1)
		{
			callback.onCreateException(sender, args, new RewardException("Too many arguments. Expected the amount of money."));
			return;
		}

		if (money <= 0)
		{
			callback.onCreateException(sender, args, new RewardException("Expected a number, larget than zero."));
		}

		callback.onCreate(sender, args, new MoneyReward(this, money));
	}

	@Override
	public String args()
	{
		return "<money>";
	}

	@Override
	public String[] help()
	{
		return new String[] { "Specify some amount of money." };
	}

	String format(double money)
	{
		return Integer.toString((int) money);
	}

	void depositPlayer(String name, double money) throws RewardException
	{
		try
		{
			final RPUser user = plugin.getUserProvider().getUserByName(name);

			new UpdateUserBalanceQuery(user, (int) money, plugin.getUserProvider())
			{
				@Override
				public void onSuccess()
				{
					ChatUtil.sendMessage(Bukkit.getPlayer(name), "Vous venez de recevoir {%1$s} pièces d'or.", money);
				}

				@Override
				public void onException(HeavenException ex)
				{
					ChatUtil.sendMessage(Bukkit.getPlayer(name), ex.getMessage());
				}
			}.schedule();
		}
		catch (final HeavenException ex)
		{
			throw new RewardException(ex);
		}
	}
}